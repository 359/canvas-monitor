const express = require('express');
const app = express();

app.use(express.static(__dirname + ''));

/**
 * returns random Integer from interval
 * @param {int} min 
 * @param {int} max 
 */
function getRandomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}


/**
 * returns random array element
 * @param {Array} array 
 */
function randomEnum(array) {
    return array[getRandomIntFromInterval(0, array.length-1)]
}

const lightValues  = [ 'on', 'off' ],
      pumpStatuses = ['ok', 'normal', 'alert' ],
      pumpStates   = [ 'manual', 'auto'];
/**
 * returns random sample data for front-end
 */
function getRandomData() {
    return {
        data: {
            "boiler": {
                "top": getRandomIntFromInterval(0, 180),
                "sub": getRandomIntFromInterval(0, 180),
                "inner": {
                    "gauge": {
                        "min": 0,
                        "max": 100,
                        "current": getRandomIntFromInterval(0, 100),
                        "threshold": getRandomIntFromInterval(0, 100)
                    },
                    "high": {
                        "light": randomEnum(lightValues),
                        "value": getRandomIntFromInterval(0, 180)
                    },
                    "medium": {
                        "light": randomEnum(lightValues),
                        "value": getRandomIntFromInterval(0, 180)
                    },
                    "low": {
                        "light": randomEnum(lightValues),
                        "value": getRandomIntFromInterval(0, 180)
                    }
                }
            },
            "pumps": {
                "name": "Pump name",
                "p1": {
                    "status": randomEnum(pumpStatuses),
                    "state": randomEnum(pumpStates)
                },
                "p2": {
                    "status": randomEnum(pumpStatuses),
                    "state": randomEnum(pumpStates)
                },
                "output": {
                    "m3": getRandomIntFromInterval(5, 50),
                    "pressure": getRandomIntFromInterval(100, 1000) / 100
                }
            }
        }

    }
}




app.get('/api/getData', function(req, res) {
    res.send(getRandomData())
})


console.log(`Server started at ${process.env.PORT || 8080}`);
app.listen(process.env.PORT || 8080);

