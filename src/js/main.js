var canvas = oCanvas.create({
    canvas: "#canvas",
    fps: 60,
    width: 800,
    height: 480
});

const endpoint = '/api/getData';
var   pumps,
      boiler;


 /**
  * Fetching data for first render
  */   
fetch(endpoint).then( response =>  response.json() )
                     .then( data => initCanvas(data.data) )
                     .catch( err =>   console.log(err) )


/**
 * Gets data from endpoint and reinitializes canvas every second
 */
setInterval(() => fetch(endpoint).then( res => res.json())
                    .then( res => initCanvas(res.data))
                    .catch( err => console.log(err) )
             , 1000
            )


/** Function to reinit canvas with given data */
var initCanvas = (data) =>  {    
     canvas.reset() 
     pumps = new Pumps(data.pumps)
     boiler = new BoilerCan(data.boiler)
}