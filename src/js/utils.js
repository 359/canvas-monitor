class Utils {
    constructor() {

    }

    static getColors() {
        return {
                green: '#4ffd63',
                orange: '#FF9C00',
                black: '#2a2a2a',
                red: '#ff0000',
                white: '#ffffff',
                blue: '#66a6ff'
        }
        
    }
}