class BoilerCan {
    constructor(data) {

        this.boilerData = data
        this.labelsValue = 'cm'


        this.color = 'linear - gradient(0deg, #89f7fe 0%, #66a6ff 100%);'
        this.shadow = '0 5px 5px rgba(0,0,0, 0.3)'
        this.boil = canvas.display.rectangle({
            x: canvas.width / 4,
            y: canvas.height - 50,
            origin: { x: "center", y: "bottom" },
            width: 300,
            height: 300,
            fill: this.color,
            shadow: this.shadow
        });

        this.arc = canvas.display.arc({
            x: canvas.width / 4,
            y: 330,
            radius: 250,
            start: 233,
            end: -53,
            direction: 'clockwise',
            fill: "linear-gradient(0deg, #89f7fe 0%, #66a6ff 100%);"
        });


        canvas.addChild(this.arc);

        canvas.addChild(this.boil)



        var linebar = new LineBar(this.boilerData.inner.gauge)


        this.labels = {
            label1 : new Label({ x: 225, y: 175 }, this.labelsValue, this.boilerData.inner.high.value ),
            label2 : new Label({ x: 225, y: 310 }, this.labelsValue, this.boilerData.inner.medium.value),
            label3 : new Label({ x: 225, y: 390 }, this.labelsValue, this.boilerData.inner.low.value),
            
            label4 : new Label({ x: 88, y: 119 }, this.labelsValue, this.boilerData.sub),
            label5 : new Label({ x: 93, y: 96, w: 70, h: 16, textSize: 8, color: Utils.getColors().orange }, this.labelsValue, this.boilerData.top)
        }

        this.lamps = {
            lamp1 : new Lamp({ x: 325, y: 175 }, this.boilerData.inner.high.light),
            lamp2 : new Lamp({ x: 325, y: 310 }, this.boilerData.inner.medium.light),
            lamp3 : new Lamp({ x: 325, y: 390 }, this.boilerData.inner.low.light)
        }


    }

}