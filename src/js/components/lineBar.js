class LineBar {
    constructor(gaugeData) {

        this.gaugeData = gaugeData
        /**
         * 
         * "gauge": {
                "min": 0,
                "max": 100,
                "current": 53,
                "threshold": 75
            },
         */


        this.background = canvas.display.rectangle({
            x: 87,
            y: 408,
            origin: { x: "center", y: "bottom" },
            width: 20,
            height: 250,
            fill: '#ffffff',
            shadow: '0 3px 3px rgba(0,0,0, 0.2)'
        });
        this.foreground = canvas.display.rectangle({
            x: 87,
            y: 408,
            origin: { x: "center", y: "bottom" },
            width: 20,
            height: this.background.height/100*this.gaugeData.current,
            fill: '#66a6ff'
        });
        this.triangle = canvas.display.polygon({
            x: 138,
            y: this.background.y - this.background.height/100*this.gaugeData.threshold,
            sides: 3,
            origin: {x: 'left', y: 'center'},
            radius: 20,
            rotation: 180,
            fill: '#e60000'
        });

        
        canvas.addChild(this.background)
        canvas.addChild(this.foreground)
        canvas.addChild(this.triangle)

        this.getCurrentPercentage = gaugeData => (gaugeData.max - gaugeData.min) / 100 
    }
}