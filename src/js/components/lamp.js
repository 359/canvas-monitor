class Lamp {
    constructor(pos, state = 'off') {

        this.lampGraphics = canvas.display.arc({
                x: pos.x,
                y: pos.y,
                origin: { x: "center", y: "center" },
                radius: 10,
                start: 0,
                end: 360,
                direction: 'clockwise',
                shadow: '0 3px 3px rgba(0,0,0, 0.2)',
                fill: state == 'on' ? '#f3ff22' : '#fff'  
            });

        canvas.addChild(this.lampGraphics)

    }
}