class Lines {
    constructor() {

        this.line1 = canvas.display.line({
            start: { x: 302, y: 379 },
            end: { x: 800, y: 376 },
            stroke: "3px #000000"
        })

        this.line2 = canvas.display.line({
            start: { x: 420, y: 270, },
            end: { x: 420, y: 379 },
            stroke: "3px #000000"
        })

        this.line3 = canvas.display.line({
            start: { x: 609, y: 270, },
            end: { x: 609, y: 379 },
            stroke: "3px #000000"
        })

        this.line4 = canvas.display.line({
            start: { x: 420, y: 270, },
            end: { x: 609, y: 270 },
            stroke: "3px #000000"
        })

        canvas.addChild(this.line1);
        canvas.addChild(this.line2);
        canvas.addChild(this.line3);
        canvas.addChild(this.line4);

    }
}