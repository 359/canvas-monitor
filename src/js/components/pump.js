class Pumps {
    constructor(pumpsData) {

        this.pumpsData = pumpsData

        this.colors = Utils.getColors()
        this.getPumpColor = status => {
            switch (status) {
                case 'ok':
                    return {
                        arrow: this.colors.green,
                        circle: this.colors.white
                    }
                    break;
                case 'normal':
                    return {
                        arrow: this.colors.black,
                        circle: this.colors.white
                    }
                    break;
                case
                    'alert':
                    return {
                        arrow: this.colors.black,
                        circle: this.colors.red
                    }
                    break;
                default:
                    return {}
            }
        }

        this.circle1 = canvas.display.arc({
            x: 515,
            y: 270,
            origin: { x: "center", y: "center" },
            radius: 30,
            start: 0,
            end: 360,
            direction: 'clockwise',
            shadow: '0 3px 3px rgba(0,0,0, 0.2)',
            fill: this.getPumpColor(this.pumpsData.p1.status).circle
        });


        this.circle2 = canvas.display.arc({
            x: 515,
            y: 378  ,
            origin: { x: "center", y: "center" },
            radius: 30,
            start: 0,
            end: 360,
            direction: 'clockwise',
            shadow: '0 3px 3px rgba(0,0,0, 0.2)',
            fill: this.getPumpColor(this.pumpsData.p2.status).circle
        });

        this.triangle1 = canvas.display.polygon({
            x: this.circle1.x,
            y: this.circle1.y,
            sides: 3,
            radius: 30,
            rotation: 0,
            origin: { x: "center", y: "center" },
            fill: this.getPumpColor(this.pumpsData.p1.status).arrow 
        });
        this.triangle2 = canvas.display.polygon({
            x: this.circle2.x,
            y: this.circle2.y,
            sides: 3,
            radius: 30,
            rotation: 0,
            origin: { x: "center", y: "center" },
            fill: this.getPumpColor(this.pumpsData.p2.status).arrow 
        });

        this.label1 = new PumpLabel({x: this.circle1.x, y: this.circle1.y - 45}, this.pumpsData.p1.state, 'P1')
        this.label2 = new PumpLabel({ x: this.circle2.x, y: this.circle2.y - 45 }, this.pumpsData.p2.state, 'P2')
        this.label3 = new Label({x: 523, y: 166, w: 150, h: 50, textSize: 20,
                                 color: this.colors.blue, origin: {x: 'center', y: 'center'}, 
                                textOrigin: {x: 'center', y: 'center'} }, '', this.pumpsData.name)



        var lines = new Lines()
        var label6 = new Label({
            x: 648,
            y: 363
        }, 'm3/h', this.pumpsData.output.m3)
        var label7 = new Label({
            x: 648,
            y: 393
        }, 'Bar', this.pumpsData.output.pressure)


        canvas.addChild(this.circle1)
        canvas.addChild(this.circle2)
        canvas.addChild(this.triangle1)
        canvas.addChild(this.triangle2)
       
    }
}