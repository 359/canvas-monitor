class PumpLabel {
    constructor(pos, state, name) {
        this.colors = Utils.getColors()

        this.pos = pos,
        this.state = state,
        this.name = name;

        this.name = canvas.display.text({
            origin: { x: "left", y: "center" },
            x: pos.x -40,
            y: this.pos.y,
            text: this.name,
            fill: '#000000'            
        })

        this.label = canvas.display.rectangle({
            x: pos.x -15,
            y: pos.y,
            origin: { x: "left", y: "center" },
            width: 80,
            height: 20,
            fill: this.getLabelBg(this.state),
            shadow: '0 3px 3px rgba(0,0,0, 0.2)'

        })

        this.labelText = canvas.display.text({
            origin: {x: 'center', y: 'center' },
            x: this.label.x + 40,
            y: this.label.y ,
            fill: this.colors.black,
            text: this.state
        })


        canvas.addChild(this.label) 
        canvas.addChild(this.name) 
        canvas.addChild(this.labelText) 
    }

    getLabelBg(state) {

        switch (state) {
            case 'auto':
                return this.colors.green
                break;
            case 'manual':
                return this.colors.orange
                break;
            default:
                return this.colors.white
        }
    } 

}