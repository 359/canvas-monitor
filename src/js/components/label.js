class Label {
    constructor( conf = {x, y, w, h, textSize, color}, value = 'cm', text = '130') {

        this.text = text
        this.value = value
        this.conf = {
            x : conf.x,
            y: conf.y,
            w: conf.w || 80,
            h: conf.h || 20,
            textSize: conf.textSize || 10,
            color: conf.color || Utils.getColors().white,
            origin: conf.origin || { x: "left", y: "center" },
            textOrigin: conf.textOrigin || { x: 'left', y: 'center' }
        }

        this.labelGraphics = canvas.display.rectangle({
            x: conf.x,
            y: conf.y,
            origin: this.conf.origin,
            width: this.conf.w,
            height: this.conf.h,
            fill: this.conf.color,
            shadow: '0 3px 3px rgba(0,0,0, 0.2)'
        })

        this.valueGraphics = canvas.display.text({
            align: 'right',
            text: this.value,
            size: this.conf.textSize,
            origin: {x: 'right', y: 'center'},
            x: this.labelGraphics.x + this.labelGraphics.width - 3,
            y: this.labelGraphics.y + 2,

            font: "bold sans-serif",
            fill: "#2a2a2a"
        })

        this.textGraphics = canvas.display.text({
            align: 'left',
            text: this.text,
            size: 10,
            origin: this.conf.textOrigin,
            x: this.labelGraphics.x + 15,
            y: this.labelGraphics.y + 2,
            font: "bold sans-serif",
            fill: "#2a2a2a"
        })

        this.labelGraphics.dragAndDrop()

        // label drags for fast positioning during dev
        this.labelGraphics.bind('mouseup', e => console.log(`coords are: {x: ${this.labelGraphics.x}, y: ${this.labelGraphics.y}}`))

        canvas.addChild(this.labelGraphics)
        canvas.addChild(this.valueGraphics)
        canvas.addChild(this.textGraphics)
    }

    setText (text) {
        this.text = text

    }


}